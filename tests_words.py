import gensim
import logging
from gensim.models import KeyedVectors

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
word_vectors = KeyedVectors.load_word2vec_format('./vectors.bin', binary=True)
quit = False

while not quit :

    pos_input = input("Positive word(s) (or 'EXIT' to leave)\n")
    if pos_input == 'EXIT' :
        break

    neg_input = input("Negative word(s) (or 'EXIT' to leave)\n")
    if neg_input == 'EXIT' :
        break

    pos_input = pos_input.split()
    for w in pos_input :
        print(w + " : ")
        for w_syno in word_vectors.most_similar(w, topn = 5) :
            print("    " + w_syno[0] + " accuracy : %.4f" % (w_syno[1]))

    neg_input = neg_input.split()
    for w in neg_input :
        print(w + " : ")
        for w_syno in word_vectors.most_similar(w, topn = 5) :
            print("    " + w_syno[0] + " accuracy : %.4f" % (w_syno[1]))


    words = word_vectors.most_similar(positive=pos_input, negative=neg_input, topn=5)
    print("result : ")
    for w in words :
        print(w[0] + " accuracy : %.5f" % (w[1]))