#import gensim
import math
import logging
from gensim.models import KeyedVectors

list_images_small = [
    ['water', 'liquid', 'drop', 'wet'],
    ['hot', 'fire', 'burn'],
    ['cute', 'small', 'soft'],
    ['happy', 'feeling', 'face'],
    ['read', 'book', 'write', 'word'],
    ['art', 'paint', 'sculpture'],
    ['idea', 'think', 'learn'],
    ['mechanism', 'heel', 'cog'],
    ['monster', 'giant', 'destroy'],
    ['animal', 'animals', 'pet'],
    ['male', 'man', 'manhood'],
    ['female', 'woman', 'womanhood'],
    ['bed', 'sleep', 'cosy'],
    ['food', 'eat', 'meal'],
]

# corrected after using find_sin(images)
list_images_big = [
    ['water', 'liquid', 'drop', 'wet',
    'fresh_water',
    'salt_water',
    'seawater',
    'rainwater',
    'brackish_water',
    'liquid_form',
    'liquid_nitrogen',
    'liquefied',
    'distilled_water',
    'drops',
    'fall',
    'dropped',
    'dropping',
    'damp',
    'dry',
    'wet_conditions',
    'soggy',
    'rain'],
    ['hot', 'fire', 'burn',
    'hottest',
    'sizzling',
    'blaze',
    'fires',
    'blaze_started',
    'flames',
    'blazes',
    'burning',
    'burned',
    'burnt',
    'burns',
    'burning_wood'],
    ['cute', 'kawaii', 'soft',
    'adorable',
    'cutesy',
    'dorky',
    'cute_little',
    'goofy',
    'anime',
    'softer',
    'springy',
    'gentle'
    ],
    ['happy', 'feeling', 'face',
    'very_happy',
    'glad',
    'happier',
    'excited',
    'quite_happy',
    'feel',
    'felt',
    'feels',
    'feel_like',
    'sense',
    'faces',
    'faced',
    'facing',
    'pose'
    ],
    ['read', 'book', 'write', 'word',
    'reads',
    'reading',
    'reread',
    'read_aloud',
    'rereading',
    'memoir',
    'books',
    'tome',
    'biography',
    'book_titled',
    'writing',
    'written',
    'compose',
    'wrote',
    'phrase',
    'words',
    'adjective',
    'phrases',
    'verb',
    'quote'
    ],
    ['art', 'paint', 'sculpture',
    'conceptual_art',
    'printmaking',
    'fine_art',
    'contemporary_art',
    'decorative_arts',
    'paints',
    'painted',
    'painting',
    'sculptures',
    'sculptor',
    'bronze_sculpture',
    ],
    ['idea', 'think', 'learn',
    'notion',
    'ideas',
    'concept',
    'suggestion',
    'theory',
    'know',
    'believe',
    'i_think',
    'teach',
    'learning',
    'discover',
    'learned',
    'understand'
    ],
    ['mechanism', 'wheel', 'cog',
    'mechanisms',
    'system',
    'framework',
    'process',
    'method',
    'cogs',
    'oiled',
    'machine',
    'gears'
    ],
    ['monster', 'giant', 'destroy',
    'monsters',
    'demonic',
    'godzilla',
    'beast',
    'demon',
    'gigantic',
    'behemoth',
    'giant_inflatable',
    'gargantuan',
    'towering',
    'destroying',
    'obliterate',
    'annihilate',
    ],
    ['animal', 'animals', 'pet',
    'wild_animal',
    'exotic_pets',
    'animal',
    'wild_animals',
    'farm_animals',
    'companion_animals',
    'pets',
    'dog',
    'fido',
    'canine',
    'pet_owner'
    ],
    ['male', 'man', 'manhood',
    'males',
    'teenager',
    'boy',
    'teenage_boy',
    'elderly_man',
    'masculinity',
    'machismo',
    'manliness',
    'virility'
    ],
    ['female', 'woman', 'womanhood',
    'females',
    'young_woman',
    'girl',
    'old_woman',
    'young_girl',
    'femininity',
    'womanly',
    'feminism',
    'domesticity',
    'virginal'
    ],
    ['bed', 'sleep', 'cosy',
    'futon',
    'cot',
    'pillow',
    'sleeping',
    'bunk_bed',
    'fall_asleep',
    'sleeping',
    'sleep_patterns',
    'nap',
    'restful_sleep',
    'cozy',
    'snug',
    'homely',
    'comfy'
    ],
    ['food', 'nourishment', 'eat',
    'fresh_produce',
    'nutritious_food',
    'foods',
    'foodstuffs',
    'prepared_meals',
    'sustenance',
    'nutritious_food',
    'eating',
    'overeat',
    'eat_healthy',
    'skip_meals',
    'eaten',
    'meal'
    ],
]


def correspond_words(word_vectors, list_words):
    for syn in list_words:
        for image in images:
            for word in image:
                print(word + ' ' + syn[0] + ' : ' + word_vectors.similarity(word, syn[0]))


def find_syn(word_vectors, images, top = 5):
    for image in images:
        print(image[0])
        for word in image :
            for w in word_vectors.most_similar(word, topn = top) :
                print("  " + w[0])

def ponderate_word_index(word_vectors, word, accuracy):
    if(word_vectors.index2entity.index(word) < 65):
        return 0
    return 1 / math.log(word_vectors.index2entity.index(word) * accuracy)

def find_related_words(word_vectors, word, top = 15, most_used = 5):
    list_syn = word_vectors.most_similar(word, topn = top)

    if most_used <= 0:
        return list_syn

    list_syn_pond = []
    for syn in list_syn:
        list_syn_pond.append( (syn[0], ponderate_word_index(word_vectors, syn[0], syn[1])) )

    #print("for '" + word + "' the synonyms are : ")
    #print(list_syn_pond)

    list_most_important_words = sorted(list_syn_pond, key=lambda key:key[1], reverse=True)[:most_used]

    #print("most importants : ")
    #print(list_most_important_words)
    return list_most_important_words

def correspond_words_to_image(word_vectors, words, images, threshold = 25000):
    for word in words:
        #print("for word : " + word)
        images_list = []
        i = -1
        for image in images:
            i += 1
            print('\nfor image : ' + (str)(i) )
            image_score = 0
            for w in image:
                print(w , end = "")
                image_score += word_vectors.rank(w, word)
                print(' = ' + (str)(image_score), end=" - ")
            if(image_score > 0):
                images_list.append( (i, image_score/len(image)) )
        print("for '" + word + "' the images are : ")
        for img in sorted(images_list, key=lambda key:key[1]):
            print((str)(img[0]) + '.png ', end = "")
            if(img[1] > threshold):
                break

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
word_vectors = KeyedVectors.load_word2vec_format('./vectors.bin', binary=True)
word_vectors.init_sims()


while 1:
    word = input("\nEnter a word to correspond to images (or ctrl + C to quit) : ")

    try:
        synonyms = find_related_words(word_vectors, word, top = 20)
    except ValueEror:
        print("this word isn't in the database")
        continue
    final_concepts = []
    #print(synonyms)
    #i = 0
 #   for syn in synonyms[:len(synonyms) - 1]:
        #i += 1
        #print('word : ' + syn[0])
        #concepts = word_vectors.most_similar(positive=[word, synonyms[i]], negative=syn[0], topn=1)
  #      concepts = word_vectors.most_similar(positive=[word], negative=syn[0], topn=1)
        #print(concepts)
  #      closest_words = find_related_words(word_vectors, concepts[0][0], top = 10, most_used = 0)
        #print(closest_words)
  #      final_concepts.append(closest_words[0][0])
    #print("concepts : ")
    #print(list(set(final_concepts)))
    #correspond_words_to_image(word_vectors, list(set(final_concepts)), list_images_small)
    s = []
    for syn in synonyms:
        s.append(syn[0])
    correspond_words_to_image(word_vectors, [word], list_images_small)